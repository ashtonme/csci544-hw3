import sys
import nltk
import re

def selectTrainingData(inputFileName, outputFileName):        
    
    try:
        inputFile = open(inputFileName, mode='r')
        outputFile_it_s = open(outputFileName+"_it_s", mode='w')
        outputFile_you_re = open(outputFileName+"_you_re", mode='w')
        outputFile_they_re = open(outputFileName+"_they_re", mode='w')
        outputFile_loose = open(outputFileName+"_loose", mode='w')
        outputFile_too = open(outputFileName+"_too", mode='w')
    except FileNotFoundError:
        print("File not found.")
        return
    
    #to print status messages
    linesDone = 0
    
    #regex
    re_it_s= re.compile(r"\b(it *\' *s)\b", flags=re.IGNORECASE)
    re_its = re.compile(r"\b(its)\b", flags=re.IGNORECASE)
    re_you_re = re.compile(r"\b(you *\' *re)\b", flags=re.IGNORECASE)
    re_your = re.compile(r"\b(your)\b", flags=re.IGNORECASE)
    re_they_re = re.compile(r"\b(they *\' *re)\b", flags=re.IGNORECASE)
    re_their = re.compile(r"\b(their)\b", flags=re.IGNORECASE)
    re_loose = re.compile(r"\b(loose)\b", flags=re.IGNORECASE)
    re_lose = re.compile(r"\b(lose)\b", flags=re.IGNORECASE)
    re_to = re.compile(r"\b(to)\b", flags=re.IGNORECASE)
    re_too = re.compile(r"\b(too)\b", flags=re.IGNORECASE)
    
    #**********************EXPERIMENTAL**************************
    '''
    
    #count ngrams
    os.system("./text2wngram -n 2 -temp {} < {} > cmuOutput".format(inputFileName[:inputFileName.rfind('/')+1] , inputFileName))    
    ngramsFile = open("cmuOutput", mode='r')
    nGrams = {}
    for line in ngramsFile:
        label = None        
        if(re_it_s.search(line)):label="it's"
        elif(re_its.search(line)):label="its"
        elif(re_you_re.search(line)):label="you're"
        elif(re_your.search(line)):label="your"
        elif(re_they_re.search(line)):label="they're"
        elif(re_their.search(line)):label="their"
        elif(re_loose.search(line)):label="loose"
        elif(re_lose.search(line)):label="lose"
        elif(re_to.search(line)):label="to"
        elif(re_too.search(line)):label="too"
        else:continue
        first = line.split()[0].lower()
        second = line.split()[1].lower()
        count = int(line.split()[2])
        if(second+"|"+first in nGrams):nGrams[second+"|"+first]+=count
        else: nGrams[second+"|"+first] = count
        if(label in nGrams):nGrams[label]+=1
        else:nGrams[label]=1
    ngramsFile.close()
    
    tempFile = open("tempoutput", mode='w')
    print("NGRAM SIZE: {}".format(len(nGrams)))
    for word in nGrams:
        tempFile.write("{} : {}\n".format(word, nGrams[word]))
    tempFile.close()'''
    #*************************************************************
    
    for line in inputFile:
        
        linesDone += 1        
        
        #tokenize and tag the sentence
        words = line.split()
        taggedTokens = nltk.pos_tag(words)
        totalTokens = len(taggedTokens)

        for i in range(0, len(words)):
            
            label = None
            
            currentToken = words[i] #currentToken = taggedTokens[i][0]
            
            #find which word has been encountered in the sentence, among the 10 words we are looking for
            if(re_it_s.search(currentToken)): #if(re_it_s.search(currentToken)): #if(currentToken.lower()=="it's"):
                label = "it's"
            
            elif(re_its.search(currentToken)): #elif(re_its.search(currentToken)): #elif(currentToken.lower()=="its"):
                label = "its"            
            
            elif(re_you_re.search(currentToken)): #elif(re_you_re.search(currentToken)): #elif(currentToken.lower()=="you're"):
                label = "you're"
            
            elif(re_your.search(currentToken)): #elif(re_your.search(currentToken)): #elif(currentToken.lower()=="your"):
                label = "your"
            
            elif(re_they_re.search(currentToken)): #elif(re_they_re.search(currentToken)): #elif(currentToken.lower()=="they're"):
                label = "they're"
            
            elif(re_their.search(currentToken)): #elif(re_their.search(currentToken)): #elif(currentToken.lower()=="their"):
                label = "their"
            
            elif(re_loose.search(currentToken)): #elif(re_loose.search(currentToken)): #elif(currentToken.lower()=="loose"):
                label = "loose"
            
            elif(re_lose.search(currentToken)): #elif(re_lose.search(currentToken)): #elif(currentToken.lower()=="lose"):
                label = "lose"
            
            elif(re_to.search(currentToken)): #elif(re_to.search(currentToken)): #elif(currentToken.lower()=="to"):
                label = "to"
            
            elif(re_too.search(currentToken)): #elif(re_too.search(currentToken)): #elif(currentToken.lower()=="too"):
                label = "too"              
            
            else: continue #not a word among any of the 10, check next token
            
            #if control reaches here, it means a word of interest has been found, create a line in output file.            
            #wprev
            wprev = "**BegOfSent**"
            if(i>0):
                wprev = taggedTokens[i-1][0]                                  
            
            #wpprev
            wpprev = None
            if(i>1):
                wpprev = taggedTokens[i-2][0]
            
            #wnext
            wnext = "**EndOfSent**"
            if(i<totalTokens-1):
                wnext = taggedTokens[i+1][0]     
            
            #wnnext
            wnnext = None
            if(i<totalTokens-2):
                wnnext = taggedTokens[i+2][0]
            
            #tprev
            tprev = None
            if(i>0):
                tprev = taggedTokens[i-1][1]
            
            #tpprev
            tpprev = None
            if(i>1):
                tpprev = taggedTokens[i-2][1]
            
            #tnext
            tnext = None
            if(i<totalTokens-1):
                tnext = taggedTokens[i+1][1]
            
            #tnnext
            tnnext = None
            if(i<totalTokens-2):
                tnnext = taggedTokens[i+2][1]
            
            #write to output file
            if(label in ["it's", "its"]):
                outputFile_it_s.write("{} ".format(label))   
                if(wpprev):
                    outputFile_it_s.write("wpprev:{} ".format(wpprev))
                outputFile_it_s.write("wprev:{} ".format(wprev))
                outputFile_it_s.write("wnext:{} ".format(wnext))
                if(wnnext):
                    outputFile_it_s.write("wnnext:{} ".format(wnnext))                
                if(tpprev):
                    outputFile_it_s.write("tpprev:{} ".format(tpprev))   
                if(tprev):
                    outputFile_it_s.write("tprev:{} ".format(tprev))
                if(tnext):
                    outputFile_it_s.write("tnext:{} ".format(tnext))
                if(tnnext):
                    outputFile_it_s.write("tnnext:{} ".format(tnnext))
                #leave a line  
                outputFile_it_s.write("\n")     
            
            elif(label in ["you're", "your"]):
                outputFile_you_re.write("{} ".format(label))   
                if(wpprev):
                    outputFile_you_re.write("wpprev:{} ".format(wpprev))
                outputFile_you_re.write("wprev:{} ".format(wprev))
                outputFile_you_re.write("wnext:{} ".format(wnext))
                if(wnnext):
                    outputFile_you_re.write("wnnext:{} ".format(wnnext))                
                if(tpprev):
                    outputFile_you_re.write("tpprev:{} ".format(tpprev))   
                if(tprev):
                    outputFile_you_re.write("tprev:{} ".format(tprev))
                if(tnext):
                    outputFile_you_re.write("tnext:{} ".format(tnext))
                if(tnnext):
                    outputFile_you_re.write("tnnext:{} ".format(tnnext))
                #leave a line  
                outputFile_you_re.write("\n")  
                
            elif(label in ["they're", "their"]):
                outputFile_they_re.write("{} ".format(label))
                if(wpprev):
                    outputFile_they_re.write("wpprev:{} ".format(wpprev))
                outputFile_they_re.write("wprev:{} ".format(wprev))
                outputFile_they_re.write("wnext:{} ".format(wnext))
                if(wnnext):
                    outputFile_they_re.write("wnnext:{} ".format(wnnext))                
                if(tpprev):
                    outputFile_they_re.write("tpprev:{} ".format(tpprev))   
                if(tprev):
                    outputFile_they_re.write("tprev:{} ".format(tprev))
                if(tnext):
                    outputFile_they_re.write("tnext:{} ".format(tnext))
                if(tnnext):
                    outputFile_they_re.write("tnnext:{} ".format(tnnext))
                #leave a line  
                outputFile_they_re.write("\n")
                
            elif(label in ["loose", "lose"]):
                outputFile_loose.write("{} ".format(label))
                if(wpprev):
                    outputFile_loose.write("wpprev:{} ".format(wpprev))
                outputFile_loose.write("wprev:{} ".format(wprev))
                outputFile_loose.write("wnext:{} ".format(wnext))
                if(wnnext):
                    outputFile_loose.write("wnnext:{} ".format(wnnext))                
                if(tpprev):
                    outputFile_loose.write("tpprev:{} ".format(tpprev))   
                if(tprev):
                    outputFile_loose.write("tprev:{} ".format(tprev))
                if(tnext):
                    outputFile_loose.write("tnext:{} ".format(tnext))
                if(tnnext):
                    outputFile_loose.write("tnnext:{} ".format(tnnext))
                #leave a line  
                outputFile_loose.write("\n")
                
                
            elif(label in ["too", "to"]):
                outputFile_too.write("{} ".format(label))
                if(wpprev):
                    outputFile_too.write("wpprev:{} ".format(wpprev))
                outputFile_too.write("wprev:{} ".format(wprev))
                outputFile_too.write("wnext:{} ".format(wnext))
                if(wnnext):
                    outputFile_too.write("wnnext:{} ".format(wnnext))                
                if(tpprev):
                    outputFile_too.write("tpprev:{} ".format(tpprev))   
                if(tprev):
                    outputFile_too.write("tprev:{} ".format(tprev))
                if(tnext):
                    outputFile_too.write("tnext:{} ".format(tnext))
                if(tnnext):
                    outputFile_too.write("tnnext:{} ".format(tnnext))
                #leave a line  
                outputFile_too.write("\n")
              
            #***********************EXPERIMENTAL******************************
            '''outputDirectory.write("test:{}_{} ".format(tprev, tnext))'''
            #*****************************************************************             
        
        if(linesDone%10000==0):                
            print("Lines Done: {}".format(linesDone))
        
    inputFile.close()
    outputFile_it_s.close()
    outputFile_you_re.close()
    outputFile_they_re.close()
    outputFile_too.close()
    outputFile_too.close()


def main():    
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 2):
        print("Argument missing. Correct syntax is \n python3 trainingDataFormatter.py <inputFile> <outputFilename>")    
    elif(numOfArgs > 2):
        print("Too many arguments. Correct syntax is \n python3 trainingDataFormatter.py <inputFile> <outputFilename>")
    else:
        inputFileName = sys.argv[1];
        outputFileName = sys.argv[2];
        selectTrainingData(inputFileName, outputFileName);

if __name__ == '__main__':main()