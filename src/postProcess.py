import sys
import re

def postProcess(testFileName, classifierOutputFileName, correctedTestFileName, wordToCheck):    
    try:
        testFile = open(testFileName, mode='r')
        classifierOutputFile = open(classifierOutputFileName, mode='r')     
        correctedTestFile = open(correctedTestFileName, mode='w')   
    except:
        print("File not found.")
        return
    
    #regex
    re_it_s= re.compile(r"\b(it *\' *s)\b", flags=re.IGNORECASE)
    re_its = re.compile(r"\b(its)\b", flags=re.IGNORECASE)
    re_you_re = re.compile(r"\b(you *\' *re)\b", flags=re.IGNORECASE)
    re_your = re.compile(r"\b(your)\b", flags=re.IGNORECASE)
    re_they_re = re.compile(r"\b(they *\' *re)\b", flags=re.IGNORECASE)
    re_their = re.compile(r"\b(their)\b", flags=re.IGNORECASE)
    re_loose = re.compile(r"\b(loose)\b", flags=re.IGNORECASE)
    re_lose = re.compile(r"\b(lose)\b", flags=re.IGNORECASE)
    re_to = re.compile(r"\b(to)\b", flags=re.IGNORECASE)
    re_too = re.compile(r"\b(too)\b", flags=re.IGNORECASE)
    
    linesDone = 0
    changesMade = 0
    totalOccurrences = 0
    for line in testFile:
        linesDone += 1
        words = line.split()
        for i in range(0, len(words)):
            word=words[i]
            actualWord = None
            start=-1
            end=-1
            
            '''if(word.lower()=="it's"):actualWord="it's" #re_it_s.search(word).group(0)
            elif(word.lower()=="its"):actualWord="its" #re_its.search(word).group(0)
            elif(word.lower()=="you're"):actualWord="you're" #re_you_re.search(word).group(0)
            elif(word.lower()=="your"):actualWord="your" #re_your.search(word).group(0)
            elif(word.lower()=="they're"):actualWord="they're" #re_they_re.search(word).group(0)
            elif(word.lower()=="their"):actualWord="their" #re_their.search(word).group(0)
            elif(word.lower()=="loose"):actualWord="loose" #re_loose.search(word).group(0)
            elif(word.lower()=="lose"):actualWord="lose" #re_lose.search(word).group(0)
            elif(word.lower()=="to"):actualWord="to" #re_to.search(word).group(0)
            elif(word.lower()=="too"):actualWord="too" #re_too.search(word).group(0)
            else:continue
            correctedTestFile.write("{}\n".format(word))
            predicted = next(classifierOutputFile)
            if(predicted!=actualWord):
                words[i]=predicted'''
            
            if(wordToCheck=="it's" and re_it_s.search(word)):
                actualWord="it's" #re_it_s.search(word).group(0)
                start = re_it_s.search(word).start()
                end = re_it_s.search(word).end()
            elif(wordToCheck=="it's" and re_its.search(word)):
                actualWord="its" #re_its.search(word).group(0)
                start = re_its.search(word).start()
                end = re_its.search(word).end()
            elif(wordToCheck=="you're" and re_you_re.search(word)):
                actualWord="you're" #re_you_re.search(word).group(0)
                start = re_you_re.search(word).start()
                end = re_you_re.search(word).end()
            elif(wordToCheck=="you're" and re_your.search(word)):
                actualWord="your" #re_your.search(word).group(0)
                start = re_your.search(word).start()
                end = re_your.search(word).end()
            elif(wordToCheck=="they're" and re_they_re.search(word)):
                actualWord="they're" #re_they_re.search(word).group(0)
                start = re_they_re.search(word).start()
                end = re_they_re.search(word).end()
            elif(wordToCheck=="they're" and re_their.search(word)):
                actualWord="their" #re_their.search(word).group(0)
                start = re_their.search(word).start()
                end = re_their.search(word).end()
            elif(wordToCheck=="loose" and re_loose.search(word)):
                actualWord="loose" #re_loose.search(word).group(0)
                start = re_loose.search(word).start()
                end = re_loose.search(word).end()
            elif(wordToCheck=="loose" and re_lose.search(word)):
                actualWord="lose" #re_lose.search(word).group(0)
                start = re_lose.search(word).start()
                end = re_lose.search(word).end()
            elif(wordToCheck=="too" and re_to.search(word)):
                actualWord="to" #re_to.search(word).group(0)
                start = re_to.search(word).start()
                end = re_to.search(word).end()
            elif(wordToCheck=="too" and re_too.search(word)):
                actualWord="too" #re_too.search(word).group(0)
                start = re_too.search(word).start()
                end = re_too.search(word).end()
            else:continue            
            
            predicted = next(classifierOutputFile).strip().split()[0]
            wordThatAppears = word[start:end]            
            
            totalOccurrences += 1
            
            if(predicted!=actualWord):
                changesMade += 1
                for j in range(0, min(len(predicted), len(wordThatAppears))):
                    if(wordThatAppears[j].isupper()):predicted=predicted[:j]+predicted[j].upper()+predicted[(j+1):]
                words[i] = word[:start]+"{}".format(predicted)+word[end:]
                
        for word in words:
            correctedTestFile.write("{} ".format(word)) 
        correctedTestFile.write("\n")  
        if(linesDone%10000==0):                
            print("Lines Done: {}".format(linesDone))
        
    print("Changes made in {}: {}/{}".format(wordToCheck, changesMade, totalOccurrences))
    
    testFile.close()
    classifierOutputFile.close()
    correctedTestFile.close()
    
    
def main():    
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 4):
        print("Argument missing. Correct syntax is \n python3 postProcess.py <testFile> <classifierOutput> <correctedTestFile> <word>")    
    elif(numOfArgs > 4):
        print("Too many arguments. Correct syntax is \n python3 postProcess.py <testFile> <classifierOutput> <correctedTestFile>  <word>")
    else:
        testFileName = sys.argv[1]
        classifierOutputFileName = sys.argv[2]
        correctedTestFileName = sys.argv[3]
        wordToCheck = sys.argv[4]
        postProcess(testFileName, classifierOutputFileName, correctedTestFileName, wordToCheck);

if __name__ == '__main__':main()