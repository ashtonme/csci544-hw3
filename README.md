***
# Application of a perceptron to perform Homophone error correction in text.
***

### About

Homphones are words which sound similar but which have different meanings. Often, in text, we come accross such instances where the homophone of a word is wrongly used instead of the word itself. For example, often 'it's' is used in place of 'its', 'they're' is used in place of 'their' and so on. Depending on the context, this can give rise to confusion when it comes to the symantics of the sentence. Here, I have attempted to build a classifier which uses text extracted from Wikipedia dumps in order to train itself to correct errors involving the following word-pairs: it's vs its, you're vs your, they're vs their, loose vs lose and to vs too.

***

### Approach

First, raw text was extracted from Wikipedia dumps available online. Then, instances involving the words it's ,its, you're ,your, they're ,their, loose ,lose, to or too were separated and a training set was constructed with the following features: previous word, next word, previous-to-previous word, next-to-next word, and the POS tags for each of these words. Then the formatted training set was fed to megaM (http://www.umiacs.umd.edu/~hal/megam/), which then constructs a model for classification. Finally, the model was used to predict the corrections on the test set and the appropriate corrections were made on the test files.

### How to execute

You would need to install nltk (please refer http://www.nltk.org/install.html) and would also require the executable binary for megaM (http://hal3.name/megam/megam_i686.opt.gz). 

To create the input for the perceptron (run separately for training set and dev set), use:

python3 trainingDataFormatter.py <inputFile> <outputFile>

To construct the model, use:

(cat <formattedTrainingSet> && echo "DEV" && cat <formattedDevSet>) > megaMInput && <pathToMegaM>/megam_i686.opt -nc multitron megaMInput > megamModel

To format the test data, use:

python3 testDataFormatter.py <testFile> <outputFile>

To use megaM to perform classification, use:

<pathToMegaM>/megam_i686.opt -nc -predict megamModel multitron <formattedTestSet> > megaMResults

To write corrections to the test file, use:

python3 postProcess.py <testFile> megaMResults <outputFile> <wordToCorrect>

### Third-party software used

* Natural Language Toolkit 3.0 (http://www.nltk.org/)

* MegaM (http://www.umiacs.umd.edu/~hal/megam/)
